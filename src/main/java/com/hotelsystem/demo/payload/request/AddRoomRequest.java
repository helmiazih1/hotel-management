package com.hotelsystem.demo.payload.request;

import javax.validation.constraints.*;

public class AddRoomRequest {

    @NotEmpty(message = "Room type cannot be empty")
    private String room_type;

    @NotEmpty(message = "Room description cannot be empty")
    private String room_desc;

    @NotNull(message = "Price cannot be empty")
    private Double price;

    @NotEmpty(message = "Room number cannot be empty")
    private String roomno;

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getRoom_desc() {
        return room_desc;
    }

    public void setRoom_desc(String room_desc) {
        this.room_desc = room_desc;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getroomno() {
        return roomno;
    }

    public void setroomno(String roomno) {
        this.roomno = roomno;
    }
}
