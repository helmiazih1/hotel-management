package com.hotelsystem.demo.payload.request;

import java.util.Set;


import javax.validation.constraints.*;

public class SignUpRequest {

    @NotEmpty(message = "Your username cannot be empty")
    @Size(min = 3, max = 40, message = "Your username must be between 8 and 30 characters")
    private String username;

    @NotEmpty(message = "Your email cannot be empty")
    @Email(message = "Please enter valid email address")
    private String email;

    private Set<String> role;

    @NotNull
    @ValidPassword
    @Size(min = 6, max = 40, message = "Password must contain at least 6 characters")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRole() {
        return this.role;
    }

    public void setRole(Set<String> role) {
        this.role = role;
    }
}
