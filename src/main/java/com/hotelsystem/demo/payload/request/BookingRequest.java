package com.hotelsystem.demo.payload.request;

import javax.validation.constraints.NotNull;

public class BookingRequest {
    

    private Long room_id;

    @NotNull(message = "Start date cannot be empty!")
    private java.sql.Date startdate;

    @NotNull(message = "End date cannot be empty!")
    private java.sql.Date enddate;

    public Long getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Long room_id) {
        this.room_id = room_id;
    }

    public java.sql.Date getStartdate() {
        return startdate;
    }

    public void setStartdate(java.sql.Date startdate) {
        this.startdate = startdate;
    }

    public java.sql.Date getEnddate() {
        return enddate;
    }

    public void setEnddate(java.sql.Date enddate) {
        this.enddate = enddate;
    }

}
