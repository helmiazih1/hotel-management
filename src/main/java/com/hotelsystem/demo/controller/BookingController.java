package com.hotelsystem.demo.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.validation.Valid;

import com.hotelsystem.demo.model.Booking;
import com.hotelsystem.demo.model.User;
import com.hotelsystem.demo.payload.request.BookingRequest;
import com.hotelsystem.demo.payload.response.MessageResponse;
import com.hotelsystem.demo.repository.BookingRepository;
import com.hotelsystem.demo.security.service.UserDetailsImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/book")
public class BookingController {

    @Autowired
    private BookingRepository repo;

    //Add new booking
    @PostMapping("/add-booking")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> addRoom(@Valid @RequestBody BookingRequest bookingRequest, Errors errors,@AuthenticationPrincipal UserDetailsImpl userDetailsImpl ) {
        if (errors.hasFieldErrors()) {
            List<FieldError> fieldError = errors.getFieldErrors();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(fieldError);
        }
        Booking book = new Booking(userDetailsImpl.getId(),bookingRequest.getRoom_id(),bookingRequest.getStartdate(),bookingRequest.getEnddate());
        repo.save(book);
        return ResponseEntity.ok(new MessageResponse("Booking Successful"));
    }

    @GetMapping("/list-booking")
    @PreAuthorize("hasRole('USER') or hasRole('RECEPTIONIST') or hasRole('ADMIN')")
    public List<Booking> getBooking(@AuthenticationPrincipal UserDetailsImpl userDetailsImpl ) {
        return repo.findByUserid(userDetailsImpl.getId());
    }
}
