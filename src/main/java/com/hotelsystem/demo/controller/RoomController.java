package com.hotelsystem.demo.controller;

import java.util.List;

import javax.validation.Valid;

import com.hotelsystem.demo.model.Room;
import com.hotelsystem.demo.payload.request.AddRoomRequest;
import com.hotelsystem.demo.payload.response.MessageResponse;
import com.hotelsystem.demo.repository.RoomRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/room")
public class RoomController {

    @Autowired
    private RoomRepository repo;

    // Get All Room
    @GetMapping("/all")
    @PreAuthorize("hasRole('USER') or hasRole('RECEPTIONIST') or hasRole('ADMIN')")
    public List<Room> getAllRoom() {
        return repo.findAll();
    }

    // Get room by id
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('RECEPTIONIST') or hasRole('ADMIN')")
    public Room getRoomById(@PathVariable(value = "id") long id) {
        return repo.findById(id);
    }

    //Add new room
    @PostMapping("/add-room")
    @PreAuthorize("hasRole('RECEPTIONIST') or hasRole('ADMIN')")
    public ResponseEntity<?> addRoom(@Valid @RequestBody AddRoomRequest addRoomRequest, Errors errors) {
        if (errors.hasFieldErrors()) {
            List<FieldError> fieldError = errors.getFieldErrors();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(fieldError);
        }
        if (repo.existsByRoomno(addRoomRequest.getroomno())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: That room already existed!"));
		}
        Room newRoom = new Room(addRoomRequest.getRoom_type(), addRoomRequest.getRoom_desc(), addRoomRequest.getPrice(),
                addRoomRequest.getroomno());

        repo.save(newRoom);
        return ResponseEntity.ok(new MessageResponse("New room successfully added!"));
    }

    //Delete room
    @DeleteMapping("/remove/{id}")
    public ResponseEntity<MessageResponse> deleteRemove(@PathVariable(value = "id") int id)
    {
        repo.deleteById(id);
        return ResponseEntity.ok(new MessageResponse("Room successfully removed!"));
    }
}
