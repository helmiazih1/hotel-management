package com.hotelsystem.demo.repository;

import java.util.Optional;

import com.hotelsystem.demo.model.ERole;
import com.hotelsystem.demo.model.Role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
}
