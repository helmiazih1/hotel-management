package com.hotelsystem.demo.repository;

import java.util.List;

import com.hotelsystem.demo.model.Room;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Long> {
    Room findById(long id);

    List<Room> findAll();

    void deleteById(long id);

    Boolean existsByRoomno(String roomno);
}