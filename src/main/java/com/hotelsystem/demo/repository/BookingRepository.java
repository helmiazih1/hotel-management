package com.hotelsystem.demo.repository;

import java.util.List;

import com.hotelsystem.demo.model.Booking;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<Booking, Long> {
    List<Booking> findByUserid(long id);
}
