package com.hotelsystem.demo.model;

public enum ERole {
	ROLE_USER,
    ROLE_RECEPTIONIST,
    ROLE_ADMIN
}