package com.hotelsystem.demo.model;

import java.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "rooms", uniqueConstraints = { @UniqueConstraint(columnNames = "roomno") })
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 20)
    private String room_type;

    @NotNull
    @Size(max = 200)
    private String room_desc;

    @NotNull
    private Double price;

    @NotNull
    @Size(max = 120)
    private String roomno;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Room(String room_type, String room_desc, Double price, String roomno) {
        this.room_type = room_type;
        this.room_desc = room_desc;
        this.price = price;
        this.roomno = roomno;
    }

    public Room() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return room_type;
    }

    public void setType(String room_type) {
        this.room_type = room_type;
    }

    public String getDesc() {
        return room_desc;
    }

    public void setDesc(String room_desc) {
        this.room_desc = room_desc;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getroomno() {
        return roomno;
    }

    public void setroomno(String roomno) {
        this.roomno = roomno;
    }

}
