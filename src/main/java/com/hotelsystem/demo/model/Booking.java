package com.hotelsystem.demo.model;

import java.sql.Date;
import java.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "bookings")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long userid;

    @NotNull
    private Long room_id;

    @NotNull
    private java.sql.Date startdate;

    @NotNull
    private java.sql.Date enddate;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;


    public Booking(@NotNull Long userid, @NotNull Long room_id, Date startdate, Date enddate) {
        this.userid = userid;
        this.room_id = room_id;
        this.startdate = startdate;
        this.enddate = enddate;
    }

    public Booking() {

    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public Long getUser_id() {
        return userid;
    }

    public void setUser_id(Long userid) {
        this.userid = userid;
    }



    public Long getRoom_id() {
        return room_id;
    }



    public void setRoom_id(Long room_id) {
        this.room_id = room_id;
    }



    public java.sql.Date getStartdate() {
        return startdate;
    }



    public void setStartdate(java.sql.Date startdate) {
        this.startdate = startdate;
    }



    public java.sql.Date getEnddate() {
        return enddate;
    }



    public void setEnddate(java.sql.Date enddate) {
        this.enddate = enddate;
    }



    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }



    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }



    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }



    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

   
}
